# Simple app to display posts utilising MVP pattern using https://jsonplaceholder.typicode.com/.
* Android app that loads posts and displays them in RecycleView as cards. 
* Clicking on post (body text) opens up details about it in a new activity - comments displayed below
* App retains state when it is reloaded - every succesfull post network request is cached
* Uses RXJava, Retrofit for webservice, MVP pattern
* Has tests covering the MVP part and datamodel, and some espresso integration tests with webservice. 
* Limited error handling (it's a poc)
* Bonus: displays generated avatar images from avatars.adorable.io for the users


## How to use:
* Press the floating action button to refresh data.
* Press on Post to view details and comments about it.
* app-debug.apk is in base directory for quick plug and play.
 

## Improvements and tradeoffs:
* Improve error handling messaging - exceptions should not be visible to end user
* Increase testing coverage.
* Use Dagger for DI (code has been architected to support Dagger with minimal effort due to MVP)
* Improve Javadoc
* Various other improvements and edge cases (production ready) etc