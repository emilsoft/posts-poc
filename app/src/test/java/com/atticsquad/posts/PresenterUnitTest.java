package com.atticsquad.posts;

import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.model.User;
import com.atticsquad.posts.mvp.CommentsMVPContract;
import com.atticsquad.posts.mvp.PostsMVPContract;
import com.atticsquad.posts.mvp.PostsPresenter;
import com.atticsquad.posts.mvp.UserMVPContract;
import com.atticsquad.posts.mvp.UserPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.Stubber;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Tests for the MVP presenter functionality ensuring data is propagated correctly
 * and as expected between the MVP layers
 */
public class PresenterUnitTest {
    private PostsMVPContract.View mPostsMockView;
    private PostsMVPContract.Interactor mPostsMockStorage;
    private PostsMVPContract.Presenter mPostsPresenterToTest;

    private UserMVPContract.View mUserMockView;
    private UserMVPContract.Interactor mUserMockStorage;
    private UserMVPContract.Presenter mUserPresenterToTest;

    @Before
    public void setup() {
        mPostsMockView = mock(PostsMVPContract.View.class);
        mPostsMockStorage = mock(PostsMVPContract.Interactor.class);
        mPostsPresenterToTest = new PostsPresenter(mPostsMockView, mPostsMockStorage);

        mUserMockView = mock(UserMVPContract.View.class);
        mUserMockStorage = mock(UserMVPContract.Interactor.class);
        mUserPresenterToTest = new UserPresenter(mUserMockView, mUserMockStorage);
    }

    //test correct MVP functionality of Posts
    @Test
    public void testSequencePost() {

        List<Post> posts = new LinkedList<>();

        // mock up the first callback
        Stubber stub = Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) throws Throwable {
                ((PostsPresenter) invocation.getArguments()[0]).onPostsRetrieved(posts);
                return null;
            }
        });

        //fire off some requests to get posts via model..
        stub.when(mPostsMockStorage).getPosts(Mockito.isA(PostsPresenter.class));
        stub.when(mPostsMockStorage).getPosts(Mockito.isA(PostsPresenter.class), Mockito.isA(Integer.class));

        //ensure that the correct instance we mocked previously is being propagated to view from presenter correctly.
        //it should be twice
        mPostsPresenterToTest.onGetPosts();
        mPostsPresenterToTest.onGetPosts(2);
        verify(mPostsMockView, times(2)).displayPosts(posts);

        //lets stub a throwable..
        Throwable throwable = new Throwable("mockito");
        Stubber stubError = Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) throws Throwable {
                ((PostsPresenter) invocation.getArguments()[0]).onProcessError(throwable);
                return null;
            }
        });

        //check that correct error message is propagated from mode>presenter to view
        stubError.when(mPostsMockStorage).getPosts(Mockito.isA(PostsPresenter.class));
        mPostsPresenterToTest.onGetPosts();
        verify(mPostsMockView, times(1)).displaySearchError(throwable.getMessage());

    }

    //test correct MVP functionality of User
    @Test
    public void testSequenceUser() {

        User user = new User();

        // mock up the first callback
        Stubber stub = Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) throws Throwable {
                ((UserPresenter) invocation.getArguments()[0]).onUserRetrieved(user);
                return null;
            }
        });

        //fire off some requests to get user via model..
        stub.when(mUserMockStorage).getUser(Mockito.isA(UserPresenter.class), eq(0));

        //ensure that the correct instance we mocked previously is being propagated to view from presenter correctly.
        //it should be twice
        mUserPresenterToTest.onGetUser(0);
        verify(mUserMockView, times(1)).displayUser(user);

        //lets stub a throwable..
        Throwable throwable = new Throwable("mockito");
        Stubber stubError = Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) throws Throwable {
                ((UserPresenter) invocation.getArguments()[0]).onProcessError(throwable);
                return null;
            }
        });

        //check that correct error message is propagated from mode;>presenter to view
        stubError.when(mUserMockStorage).getUser(Mockito.isA(UserPresenter.class),eq(0));
        mUserPresenterToTest.onGetUser(0);
        verify(mUserMockView, times(1)).displaySearchError(throwable.getMessage());

    }


}