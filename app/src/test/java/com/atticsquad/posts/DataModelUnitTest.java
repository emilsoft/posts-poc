package com.atticsquad.posts;

import com.atticsquad.posts.model.Comment;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.model.User;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Local data unit test of model
 */
public class DataModelUnitTest {


    @Test
    public void testPostDataModel() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("post.json");
        File file = new File(classLoader.getResource("post.json").getFile());
        FileInputStream inputStream = new FileInputStream(file);

        assertNotNull(resource);

        Gson gson = new Gson();
        Post[] response = gson.fromJson(IOUtils.toString(inputStream), Post[].class);
        assertNotNull(response);

        assertEquals(response.length, 100);
        assertEquals(response[2].getTitle(), "ea molestias quasi exercitationem repellat qui ipsa sit aut");
        assertEquals(response[10].getBody(), "delectus reiciendis molestiae occaecati non minima eveniet qui voluptatibus\n" +
                "accusamus in eum beatae sit\n" +
                "vel qui neque voluptates ut commodi qui incidunt\n" +
                "ut animi commodi");

    }


    @Test
    public void testCommentDataModel() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("comment.json");
        File file = new File(classLoader.getResource("comment.json").getFile());
        FileInputStream inputStream = new FileInputStream(file);

        assertNotNull(resource);

        Gson gson = new Gson();
        Comment[] response = gson.fromJson(IOUtils.toString(inputStream), Comment[].class);
        assertNotNull(response);


        assertEquals(response.length, 500);
        assertEquals(response[65].getName(), "eius nam consequuntur");
        assertEquals(response[149].getEmail(), "Ramiro_Kuhn@harmon.biz");
        assertTrue(response[149].getPostId() == 30);
    }


    @Test
    public void testUserDataModel() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("user.json");
        File file = new File(classLoader.getResource("user.json").getFile());
        FileInputStream inputStream = new FileInputStream(file);

        assertNotNull(resource);

        Gson gson = new Gson();
        User response = gson.fromJson(IOUtils.toString(inputStream), User.class);
        assertNotNull(response);


        assertEquals(response.getName(), "Leanne Graham");
        assertEquals(response.getEmail(), "Sincere@april.biz");
        assertEquals(response.getPhone(), "1-770-736-8031 x56442");
        assertTrue(response.getId() == 1);
    }

}