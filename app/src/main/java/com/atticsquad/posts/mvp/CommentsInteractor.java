package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.Comment;
import com.atticsquad.posts.service.CommentService;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Gets comments data from the datamodel (in this case service).
 * Always tries to get freshest data from service, and it does not cache the comments.
 */
public class CommentsInteractor extends BaseInteractor implements CommentsMVPContract.Interactor {

    private CommentsMVPContract.Interactor.ServiceListener mListener;

    private CommentService mRequestInterface;

    public CommentsInteractor(CommentService requestInterface) {
        mRequestInterface = requestInterface;
    }


    @Override
    public void getComments(CommentsMVPContract.Interactor.ServiceListener listener, int userId) {
        mListener = listener;
        mListener.onProcessStart();
        getComments(userId);
    }


    @Override
    public void onDestroy() {
        disposeCompositeDisposable();
        mRequestInterface = null;
    }


    private void getComments(int userId) {

        createCompositeDisposable();

        mCompositeDisposable.add(mRequestInterface.getComments(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onCommentsRetrieved, this::onProcessError));
    }


    public void onCommentsRetrieved(List<Comment> commentsList) {
        mListener.onCommentsRetrieved(commentsList);
    }

    public void onProcessError(Throwable error) {
        mListener.onProcessError(error);
    }


}
