package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.Post;

import java.util.List;

public class PostsMVPContract {

    //aka the model
    public interface Interactor {

        void getPosts(InteractorListener listener);

        void getPosts(InteractorListener listener, int userId);

        List<Post>  getCachedData(InteractorListener listener);

        void onDestroy();

        interface InteractorListener {
            void onPostsRetrieved(List<Post> postList);

            void onProcessError(Throwable error);

            void onProcessStart();
        }

    }

    public interface View {

        void displayPosts(List<Post> posts);

        void displaySearchError(String errorMessage);

        void displayIsLoadingData();

    }

    public interface Presenter {

        //accessed from view
        void onGetPosts();

        //accessed from view
        void onGetPosts(int userId);

        //accessed from view
        List<Post> onGetCachedPosts();

        //accessed from view
        void onDestroy();

    }
}
