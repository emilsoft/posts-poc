package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.User;


public class UserMVPContract {

    //aka the model
    public interface Interactor {

        void getUser(UserMVPContract.Interactor.ServiceListener listener, int userId);

        void onDestroy();

        interface ServiceListener {

            void onUserRetrieved(User users);

            void onProcessError(Throwable error);

            void onProcessStart();
        }

    }

    public interface View {

        void displayUser(User user);

        void displaySearchError(String errorMessage);

        void displayIsLoadingData();

    }

    public interface Presenter {

        //from view
        void onGetUser(int userId);

        //accessed from view
        void onDestroy();

    }
}
