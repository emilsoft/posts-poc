package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.User;
import com.atticsquad.posts.service.UserService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Gets users data from the datamodel (in this case service).
 * Always tries to get freshest data from service.
 */
public class UserInteractor extends BaseInteractor implements UserMVPContract.Interactor {

    private ServiceListener mListener;

    private UserService mRequestInterface;

    public UserInteractor(UserService requestInterface) {
        mRequestInterface = requestInterface;
    }


    @Override
    public void getUser(ServiceListener listener, int userId) {
        mListener = listener;
        mListener.onProcessStart();
        getUser(userId);
    }



    @Override
    public void onDestroy() {
        disposeCompositeDisposable();
        mRequestInterface =null;
    }


    private void getUser(int userId) {

        createCompositeDisposable();

        mCompositeDisposable.add(mRequestInterface.getUser(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onUserRetrieved, this::onProcessError));
    }


    public void onUserRetrieved(User user) {
        mListener.onUserRetrieved(user);
    }

    public void onProcessError(Throwable error) {
        mListener.onProcessError(error);
    }


}
