package com.atticsquad.posts.mvp;

import android.util.Log;

import com.atticsquad.posts.model.Cache;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.service.PostService;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Gets post data from the datamodel (in this case web service).
 * Always tries to get freshest data from service, if not accessible it will try and show cached data (if there is any available)
 */
public class PostsInteractor extends BaseInteractor implements PostsMVPContract.Interactor {

    private static final String TAG = PostsInteractor.class.getSimpleName();

    private Cache mCache;

    private InteractorListener mListener;

    private PostService mRequestInterface;

    public PostsInteractor(PostService requestInterface, Cache cache) {
        mRequestInterface = requestInterface;
        mCache = cache;
    }

    @Override
    public void getPosts(InteractorListener listener) {
        mListener = listener;
        mListener.onProcessStart();
        getPosts();
    }


    @Override
    public void getPosts(InteractorListener listener, int userId) {
        mListener = listener;
        mListener.onProcessStart();
        getPosts(userId);
    }


    private void getPosts(int userId) {

        Log.d(TAG, "Get posts " + userId);

        createCompositeDisposable();
        mCompositeDisposable.add(mRequestInterface.getPosts(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onPostsRetrieved, this::onProcessError));
    }

    private void getPosts() {

        Log.d(TAG, "Get posts ");

        createCompositeDisposable();

        mCompositeDisposable.add(mRequestInterface.getPosts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onPostsRetrieved, this::onProcessError));
    }


    private void cacheData(List<Post> posts) {
        mCache.saveMessages(posts);
    }

    @Override
    public List<Post> getCachedData(InteractorListener listener) {
        mListener = listener;
        List<Post> posts = mCache.getSavedMessages();
        if (posts != null) {
            for (Post post : posts) {
                post.cached = true;
            }
            onPostsRetrieved(posts);
        } else {
            //if no cached data try and get data from net instead
            getPosts();
        }
        return posts;
    }

    private void onPostsRetrieved(List<Post> posts) {
        cacheData(posts); //save the last data
        mListener.onPostsRetrieved(posts);
    }

    private void onProcessError(Throwable error) {
        mListener.onProcessError(error);
    }

    @Override
    public void onDestroy() {
        disposeCompositeDisposable();
        mCache.onDestroy();
        mCache = null;
        mListener = null;
        mRequestInterface = null;
    }


}
