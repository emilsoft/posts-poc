package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.Post;

import java.util.List;


public class PostsPresenter implements PostsMVPContract.Presenter, PostsMVPContract.Interactor.InteractorListener {

    private PostsMVPContract.Interactor mModel;
    private PostsMVPContract.View mView;

    //better to insert both model and login view so it's easier to mock them later for testing
    public PostsPresenter(PostsMVPContract.View view, PostsMVPContract.Interactor model) {
        this.mView = view;
        this.mModel = model;
    }

    @Override
    public void onPostsRetrieved(List<Post> postList) {
        mView.displayPosts(postList);
    }


    @Override
    public void onProcessError(Throwable error) {
        mView.displaySearchError(error.getMessage());
    }

    @Override
    public void onProcessStart() {
        mView.displayIsLoadingData();
    }

    @Override
    public void onGetPosts() {
        mModel.getPosts(this);
    }

    @Override
    public void onGetPosts(int userId) {
        mModel.getPosts(this, userId);
    }

    @Override
    public List<Post> onGetCachedPosts() {
        return mModel.getCachedData(this);
    }

    @Override
    public void onDestroy() {
        mView = null;
        mModel.onDestroy();
    }

}
