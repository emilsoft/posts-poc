package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.User;

public class UserPresenter implements UserMVPContract.Presenter, UserMVPContract.Interactor.ServiceListener {

    private UserMVPContract.Interactor mModel;
    private UserMVPContract.View mView;

    //better to insert both model and login view so it's easier to mock them later for testing
    public UserPresenter(UserMVPContract.View loginView, UserMVPContract.Interactor model) {
        this.mView = loginView;
        this.mModel = model;
    }

    @Override
    public void onGetUser(int id) {
        mModel.getUser(this, id);
    }

    @Override
    public void onDestroy() {
        mModel.onDestroy();
        mView = null;
    }

    @Override
    public void onUserRetrieved(User user) {
        mView.displayUser(user);
    }


    @Override
    public void onProcessError(Throwable error) {
        mView.displaySearchError(error.getMessage());
    }

    @Override
    public void onProcessStart() {
        mView.displayIsLoadingData();
    }
}
