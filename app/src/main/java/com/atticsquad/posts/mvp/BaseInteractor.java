package com.atticsquad.posts.mvp;

import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseInteractor {

    CompositeDisposable mCompositeDisposable;

    void createCompositeDisposable() {
        disposeCompositeDisposable();
        mCompositeDisposable = new CompositeDisposable();
    }

    void disposeCompositeDisposable() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

}
