package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.Comment;

import java.util.List;


public class CommentsMVPContract {

    //aka the model
    public interface Interactor {

        void getComments(CommentsMVPContract.Interactor.ServiceListener listener, int postId);

        void onDestroy();

        interface ServiceListener {

            void onCommentsRetrieved(List<Comment> commentsList);

            void onProcessError(Throwable error);

            void onProcessStart();
        }

    }

    public interface View {

        void displayComments(List<Comment> comments);

        void displaySearchError(String errorMessage);

        void displayIsLoadingData();

    }

    public interface Presenter {

        //from view
        void onGetComments(int postId);

        //accessed from view
        void onDestroy();

    }
}
