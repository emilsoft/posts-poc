package com.atticsquad.posts.mvp;

import com.atticsquad.posts.model.Comment;

import java.util.List;

public class CommentsPresenter implements CommentsMVPContract.Presenter, CommentsMVPContract.Interactor.ServiceListener {

    private CommentsMVPContract.Interactor mModel;
    private CommentsMVPContract.View mView;

    //better to insert both model and login view so it's easier to mock them later for testing
    public CommentsPresenter(CommentsMVPContract.View loginView, CommentsMVPContract.Interactor model) {
        this.mView = loginView;
        this.mModel = model;
    }

    @Override
    public void onGetComments(int postId) {
        mModel.getComments(this, postId);
    }

    @Override
    public void onDestroy() {
        mModel.onDestroy();
        mView = null;
    }

    @Override
    public void onCommentsRetrieved(List<Comment> commentsList) {
        mView.displayComments(commentsList);
    }

    @Override
    public void onProcessError(Throwable error) {
        mView.displaySearchError(error.getMessage());
    }

    @Override
    public void onProcessStart() {
        mView.displayIsLoadingData();
    }
}
