package com.atticsquad.posts;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.atticsquad.posts.adapter.DataAdapterPostDetails;
import com.atticsquad.posts.model.Comment;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.model.User;
import com.atticsquad.posts.mvp.CommentsInteractor;
import com.atticsquad.posts.mvp.CommentsMVPContract;
import com.atticsquad.posts.mvp.CommentsPresenter;
import com.atticsquad.posts.mvp.UserPresenter;
import com.atticsquad.posts.mvp.UserInteractor;
import com.atticsquad.posts.mvp.UserMVPContract;
import com.atticsquad.posts.service.AvatarService;
import com.atticsquad.posts.service.Client;
import com.atticsquad.posts.service.CommentService;
import com.atticsquad.posts.service.UserService;
import com.google.gson.Gson;

import java.util.List;

/**
 * Shows the comments related to a post in a recycle view.
 */
public class PostDetailsActivity extends AppCompatActivity implements CommentsMVPContract.View, UserMVPContract.View {

    private RecyclerView mRecyclerView;

    private DataAdapterPostDetails mAdapter;

    //Possible to use Dagger dependency injection for the following
    private CommentsPresenter mCommentsPresenter;
    private UserPresenter mUserPresenter;
    private AvatarService mAvatarService;

    private Post mPost;

    public static final String EXTRA_POST = "EXTRA_POST";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CommentsInteractor interactor = new CommentsInteractor(new Client<CommentService>().getService(CommentService.class));
        UserInteractor userInteractor = new UserInteractor(new Client<UserService>().getService(UserService.class));

        //bind the presenter to this view (TODO: we can use Dagger 2 dependency injection here)
        mCommentsPresenter = new CommentsPresenter(this, interactor);
        mUserPresenter = new UserPresenter(this, userInteractor);
        mAvatarService = new AvatarService();

        //setup recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        Gson gson = new Gson();
        String strObj = getIntent().getStringExtra(EXTRA_POST);
        mPost = gson.fromJson(strObj, Post.class);

        mCommentsPresenter.onGetComments(mPost.getId());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void displayComments(List<Comment> comments) {
        mAdapter = new DataAdapterPostDetails(comments,mPost,mAvatarService);
        mUserPresenter.onGetUser(mPost.getUserId());
    }

    @Override
    public void displayUser(User user) {
        Snackbar.make(mRecyclerView, "Displaying comments.. " , Snackbar.LENGTH_SHORT).setAction("Action", null).show();
        mAdapter.setUserDetails(user);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void displaySearchError(String errorMessage) {
        Snackbar.make(mRecyclerView, "Oops there's a problemo " + errorMessage, Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
    }

    @Override
    public void displayIsLoadingData() {
        Snackbar.make(mRecyclerView, "Loading data...", Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
    }

    @Override
    public void finish(){
        mCommentsPresenter.onDestroy();
        mUserPresenter.onDestroy();
        super.finish();
    }

}
