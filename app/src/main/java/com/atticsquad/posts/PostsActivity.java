package com.atticsquad.posts;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.atticsquad.posts.adapter.DataAdapterPosts;
import com.atticsquad.posts.model.Cache;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.mvp.PostsInteractor;
import com.atticsquad.posts.mvp.PostsMVPContract;
import com.atticsquad.posts.mvp.PostsPresenter;
import com.atticsquad.posts.service.AvatarService;
import com.atticsquad.posts.service.Client;
import com.atticsquad.posts.service.PostService;
import com.google.gson.Gson;

import java.util.List;

/**
 * Display post data in a list and allows individual list items to be clicked, leading to comments
 * and details for that post item.
 */
public class PostsActivity extends AppCompatActivity implements PostsMVPContract.View, DataAdapterPosts.BtnClickListener {

    private static final String TAG = PostsActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;

    private DataAdapterPosts mAdapter;

    private PostsPresenter mPresenter;
    private AvatarService mAvatarService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PostsInteractor postsInteractor = new PostsInteractor(new Client<PostService>().getService(PostService.class), new Cache(getApplicationContext()));

        //bind the presenter to this view (we can use dagger 2 dependency injection here)
        mPresenter = new PostsPresenter(this, postsInteractor);
        mAvatarService = new AvatarService();

        //setup recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.addOnScrollListener(showHideToolbarListener = new RecyclerViewUtils.ShowHideToolbarOnScrollingListener(appBar));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Snackbar.make(view, "Refreshing data...", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            Log.d(TAG, "Get posts ");
            mPresenter.onGetPosts();
        });

        //Always displayUser cached data first (if there is any) when starting the app..
        //This is a simple but effective way that covers app persistance, ensuring the last succesfull
        //request is always cached so the app loads it by default if the network is down.
        mPresenter.onGetCachedPosts();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    @Override
    public void displayPosts(List<Post> posts) {
        mAdapter = new DataAdapterPosts(posts, this,mAvatarService);
        mRecyclerView.setAdapter(mAdapter);
        Log.d(TAG, "Display posts " + posts);
        if (posts != null && posts.size() > 0 && posts.get(0).cached) {
            Snackbar.make(mRecyclerView, "Loaded cached data..", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
        } else {
            Snackbar.make(mRecyclerView, "Loaded data..", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
        }
    }


    @Override
    public void displaySearchError(String errorMessage) {
        Snackbar.make(mRecyclerView, "Oops there's a problemo " + errorMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void displayIsLoadingData() {
        Snackbar.make(mRecyclerView, "Loading data...", Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
    }

    @Override
    public void onClickedPostId(Post post) {
        //start a new activity the post has been clicked
        Gson gson = new Gson();
        Intent intent = new Intent(this, PostDetailsActivity.class);
        intent.putExtra(PostDetailsActivity.EXTRA_POST, gson.toJson(post));
        startActivity(intent);
    }

    @Override
    public void finish(){
        mPresenter.onDestroy();
        super.finish();
    }
}
