package com.atticsquad.posts.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/**
 * Cheeky cache using sharedprefs (under normal circumstance would use android internal storage,
 * but this is a poc so we will roll with it..).
 */

public class Cache {

    private static String USER_DATA = "USER_DATA";
    private static String PREF_FILE = "CHAT_PREFS";

    private WeakReference<Context> mContextWeak;

    public Cache(Context context) {
        mContextWeak = new WeakReference<Context>(context);
    }

    public List<Post> getSavedMessages() {
        Context context = mContextWeak.get();
        if (context != null) {
            SharedPreferences pref = context.getSharedPreferences(PREF_FILE, 0);
            String json = pref.getString(USER_DATA, null);
            if (json == null) {
                return null;
            } else {
                Gson gson = new Gson();
                return Arrays.asList(gson.fromJson(json, Post[].class));
            }
        } else {
            return null;
        }
    }

    public void saveMessages(List<Post> messages) {
        Context context = mContextWeak.get();
        if (context != null) {
            SharedPreferences pref = context.getSharedPreferences(PREF_FILE, 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            if (messages == null) {
                editor.remove(USER_DATA);
            } else {
                Gson gson = new Gson();
                editor.putString(USER_DATA, gson.toJson(messages));
            }
            editor.apply(); //apply is async commit
        }
    }

    public void onDestroy() {
        mContextWeak.clear();
    }
}
