package com.atticsquad.posts.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atticsquad.posts.R;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.service.AvatarService;

import java.util.List;

/**
 * List posts
 */
public class DataAdapterPosts extends RecyclerView.Adapter<DataAdapterPosts.ViewHolder> {

    private AvatarService mAvatarService;
    private List<Post> mPosts;
    private BtnClickListener mClickListener = null;

    public interface BtnClickListener {
        void onClickedPostId(Post post);
    }

    public DataAdapterPosts(List<Post> posts, BtnClickListener listener, AvatarService avatarService) {
        mPosts = posts;
        mClickListener = listener;
        mAvatarService = avatarService;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = mPosts.get(position);
        holder.mTitleText.setText(post.getTitle());
        mAvatarService.loadAvatar(post.getUserId(), holder.mAvatarImage);
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleText;
        private ImageView mAvatarImage;

        public ViewHolder(View view) {
            super(view);

            mTitleText = view.findViewById(R.id.titleTextView);
            mAvatarImage = view.findViewById(R.id.avatar);

            view.setOnClickListener(v -> {
                if (mClickListener != null) {
                    mClickListener.onClickedPostId(mPosts.get(this.getAdapterPosition()));
                }
            });


        }
    }
}
