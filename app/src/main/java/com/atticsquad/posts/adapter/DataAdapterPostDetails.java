package com.atticsquad.posts.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atticsquad.posts.R;
import com.atticsquad.posts.model.Comment;
import com.atticsquad.posts.model.Post;
import com.atticsquad.posts.model.User;
import com.atticsquad.posts.service.AvatarService;

import java.util.List;

/**
 * Use to display a detailed view of the post and it's comments.
 */
public class DataAdapterPostDetails extends RecyclerView.Adapter<DataAdapterPostDetails.ViewHolder> {

    private List<Comment> mComments;
    private Post mPost;
    private AvatarService mAvatarService;
    private User mUser;

    private static final int VIEW_TYPE_POST=0;
    private static final int VIEW_TYPE_COMMENT=1;

    public DataAdapterPostDetails(List<Comment> comments, Post post, AvatarService avatarService) {
        mComments = comments;
        mPost = post; //the post is displayed first in the list
        mAvatarService = avatarService;
    }

    public void setUserDetails(User user) {
        mUser = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_POST) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_detail_card, parent, false);
            return new ViewHolderPost(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_card, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return VIEW_TYPE_POST;
        else return VIEW_TYPE_COMMENT;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) {
            ViewHolderPost holderPost = (ViewHolderPost)holder;
            holderPost.mBodyText.setText(mPost.getBody());
            holderPost.mTitleText.setText(mPost.getTitle());
            holderPost.mNoOfCommentsText.setText(mComments.size() + "");
            if (mUser !=null) {
                holderPost.mUserNameText.setText(mUser.getName());
                mAvatarService.loadAvatar(mUser.getId(), holderPost.mAvatarImage);
            }

        } else {
            Comment comment = mComments.get(position-1);
            holder.mBodyText.setText(comment.getBody());
            holder.mTitleText.setText(comment.getName());
        }
    }

    @Override
    public int getItemCount() {
        return mComments.size()+1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mBodyText;
        TextView mTitleText;

        public ViewHolder(View view) {
            super(view);
            mBodyText = view.findViewById(R.id.bodyTextView);
            mTitleText = view.findViewById(R.id.titleTextView);
        }
    }

    class ViewHolderPost extends ViewHolder {

        TextView mUserNameText;
        TextView mNoOfCommentsText;
        ImageView mAvatarImage;

        public ViewHolderPost(View view) {
            super(view);
            mUserNameText = view.findViewById(R.id.userNameTextView);
            mNoOfCommentsText = view.findViewById(R.id.noOfCommentsTextView);
            mAvatarImage = view.findViewById(R.id.avatar);
        }

    }

}
