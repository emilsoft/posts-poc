package com.atticsquad.posts.service;

import android.widget.ImageView;

import com.atticsquad.posts.BuildConfig;
import com.squareup.picasso.Picasso;

/**
 * Loads an made up avatar image
 */
public class AvatarService {

    private static final String URL = BuildConfig.AVATAR_SERVICE_URL;

    public void loadAvatar(int id, ImageView imageView) {
        Picasso.with(imageView.getContext()).load(URL + "funkyavatar_" +id).into(imageView);
    }
}
