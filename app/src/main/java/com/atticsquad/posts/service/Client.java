package com.atticsquad.posts.service;

import com.atticsquad.posts.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Get the Service impl for the relevant call.
 */
public class Client<T> {

    //we can put this url in buildConfigField
    private static final String BASE_URL = BuildConfig.POSTS_SERVICE_URL;

    public T getService(Class<T> klass) {

        Gson gson = new GsonBuilder().setLenient().create();

        T requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build().create(klass);


        return requestInterface;
    }

}
