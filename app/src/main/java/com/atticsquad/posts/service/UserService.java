package com.atticsquad.posts.service;

import com.atticsquad.posts.model.User;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * User public service endpoint
 */
public interface UserService {

    @GET("users/{userId}")
    Observable<User> getUser(@Path("userId") int userId);

}

