package com.atticsquad.posts.service;

import com.atticsquad.posts.model.Comment;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * comments public service endpoint
 */
public interface CommentService {

    @GET("comments")
    Observable<List<Comment>> getComments(@Query("postId") int id);

}

