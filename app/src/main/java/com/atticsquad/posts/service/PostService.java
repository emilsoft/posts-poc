package com.atticsquad.posts.service;

import com.atticsquad.posts.model.Post;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * post public service endpoint
 */
public interface PostService {

    @GET("posts/")
    Observable<List<Post>> getPosts(@Query("id") int id);

    @GET("posts")
    Observable<List<Post>> getPosts();

}

