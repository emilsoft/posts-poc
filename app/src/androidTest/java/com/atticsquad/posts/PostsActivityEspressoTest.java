package com.atticsquad.posts;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.atticsquad.posts.mvp.PostsMVPContract;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Checks if data is correctly loaded and displayed in RecycleView. Also if app handles rotation correctly.
 * Integration test, the service call isn't mocked.
 */
@RunWith(AndroidJUnit4.class)
public class PostsActivityEspressoTest {

    private PostsMVPContract.Presenter mMockPresenter;

    @Before
    public void checkInitialCount() {
        rotateScreen();
        mMockPresenter = mock(PostsMVPContract.Presenter.class);
        PostsMVPContract.Presenter m = mock(PostsMVPContract.Presenter.class);
        //TBD: use dagger to be able to mock view  presenter injection
//        when(m.onGetPosts().thenReturn(mock(List<Post>[].class));
//        when(m.provideModel()).thenReturn(mock(PostsMVPContract.Interactor.class));
    }


    @Rule
    public ActivityTestRule<PostsActivity> mActivityRule =
            new ActivityTestRule<>(PostsActivity.class);

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.atticsquad.posts", appContext.getPackageName());
    }


    @Test
    public void ensureTextChangesWork() {
        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(100));
    }


    @Test
    public void clickIncrementRotateScreen() {
        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(100));
        rotateScreen();
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(100));
    }


    class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertEquals(adapter.getItemCount(), expectedCount);
        }
    }


    private void rotateScreen() {
        Context context;
        context = InstrumentationRegistry.getTargetContext();
        int orientation = context.getResources().getConfiguration().orientation;

        Activity activity = mActivityRule.getActivity();
        activity.setRequestedOrientation(
                (orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }


//    @Test
//    public void testButtonClick() {
//        //We haven't mocked the presenter in this activity still so this test will fail..
//        //use dagger to inject mocked presenter...
//        mActivityRule.launchActivity(new Intent());
//        onView(withId(R.id.fab)).perform(click());
//        verify(mMockPresenter).onGetPosts();
//    }

}